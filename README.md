## Disclaimer: 

**This is an unofficial DEB package of the Experimental adaptive branch of gpodder.**

I do not expect this package to be included in any mobile distro because, I believe that the proper way for this to be done is for the gpodder project to merge into master the adaptive branch so that there is a `blessed build` that can be uploaded into Debian and thus benefit all mobile distros.

I made this package because: 
- I wanted to practice packaging applications as DEB packages
- I want a podcast client on my Linux phone that is a DEB package and not a flatpak package

So I made this to have a deb based podcast application while I wait for the work in upstream gpodder to be concluded. Once there are official adaptive podcast applications in Debian Testing, I will delete this repo.

This was tested only on my device. And it worked. Being a non official package, you are on your own when you install this. Don’t annoy, your distro’s developers with bugs about this package. Bug me.

## Downloading the package

You can download it from here: https://git.disroot.org/maryjane/gpodder-adaptive-deb-package/raw/branch/master/gpodder_3.10.21+1-1_all.deb

## Installing

From the same directory where the package was downloaded to, run the command:

`sudo apt install ./gpodder_3.10.21+1-1_all.deb`


## Building the package from the deb source

This package was built based on the repo:

`https://git.disroot.org/maryjane/gpodder` from the `debian/latest` branch. So you can check the commit log to see every change I did to it.

The repo itself is a copy of: https://github.com/gpodder/gpodder/tree/adaptive

You can build the package yourself on a Debian based system by installing: 

`sudo apt install git build-essential devscripts`

`apt install --no-install-recommends git-buildpackage` 

Check the Build-Depends dependencies here: 

https://git.disroot.org/maryjane/gpodder/src/branch/debian/latest/debian/control#L7

And running the commands:

`git clone https://git.disroot.org/maryjane/gpodder.git`

cd into the `gpodder` directory

`git checkout debian/latest`

and run the command: 

`debuild -us -uc -b` 

## A thanks to Debian

I was able to make this repo in like 30 min, because I basically copied the `Debian`  directory from the official debian package at (https://salsa.debian.org/debian/gpodder/-/tree/master/debian) and made like 3 minor tweaks to it.

So thanks to the debian team for making the official gpodder package.

Debian rocks!!!